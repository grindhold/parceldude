/********************************************************************
# Copyright 2017 Daniel 'grindhold' Brendle
#
# This file is part of parceldude.
#
# parceldude is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# parceldude is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with parceldude.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

namespace Parceldude {
    [GtkTemplate (ui = "/de/grindhold/parceldude/ui/main.ui")]
    public class MainWindow : Gtk.ApplicationWindow {
        [GtkChild]
        private Gtk.Stack stack;

        [GtkChild]
        private Gtk.MenuButton menubutton;

        [GtkChild]
        private Gtk.Button install_all;

        private ConfigView config_view;
        private DonationsView donations_view;

        public Application app {get; set;}

        [GtkCallback]
        private void install_clicked() {
            var i = new InstallView(this.config_view);
            i.done.connect(()=>{
                this.stack.transition_type = Gtk.StackTransitionType.SLIDE_LEFT;
                this.stack.visible_child = this.donations_view;
            });
            this.stack.transition_type = Gtk.StackTransitionType.SLIDE_LEFT;
            this.stack.add(i);
            this.stack.visible_child = i;
            this.install_all.sensitive = false;
        }

        public MainWindow(Application app) {
            this.app = app;
            this.show_all();
            this.donations_view = new DonationsView();
            this.config_view = new ConfigView(this);
            this.donations_view.done.connect(()=>{
                this.stack.transition_type = Gtk.StackTransitionType.SLIDE_RIGHT;
                this.stack.visible_child = this.config_view;
            });
            this.config_view.installers_changed.connect((n)=>{
                this.install_all.sensitive = n > 0;
            });
            this.stack.add(this.donations_view);
            this.stack.add(this.config_view);
            this.stack.visible_child = this.config_view;

            var builder = new Gtk.Builder.from_resource ("/de/grindhold/parceldude/ui/menu.ui");
            var menu = builder.get_object ("main-menu") as GLib.MenuModel;
            this.menubutton.set_menu_model(menu);

            this.destroy.connect(()=>{
                Gtk.main_quit();
            });
        }
    }

    public class Application : Gtk.Application {
        private uint? mainwindow_id = null;
        public Application() {
            Object(application_id: "de.grindhold.parceldude", flags: ApplicationFlags.FLAGS_NONE);

            var about_action = new GLib.SimpleAction("about", null);
            about_action.activate.connect(()=>{this.show_about_dialog();});
            this.add_action(about_action);
        }

        private void show_about_dialog() {
            var about = new AboutDialog();
            var mw = this.get_main_window();
            if (mw != null) {
                about.set_transient_for(mw);
                about.set_modal(true);
            }
            about.show_all();
        }

        public MainWindow? get_main_window() {
            if (this.mainwindow_id != null)
                return (MainWindow)this.get_window_by_id((uint)this.mainwindow_id);
            else
                return null;
        }

        protected override void activate() {
            if (get_windows() == null)
                new_session();
        }

        private void new_session() {
            var provider = new Gtk.CssProvider ();
            provider.load_from_resource ("/de/grindhold/parceldude/application.css");
            Gtk.StyleContext.add_provider_for_screen (Gdk.Screen.get_default (), provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);

            Gtk.Settings.get_default().gtk_application_prefer_dark_theme = true;
            var mw = new MainWindow(this);
            this.add_window(mw);
            this.mainwindow_id = mw.get_id();
        }
    }

    public static void main(string[] argv) {
        Gtk.init(ref argv);

        var app = new Application();
        app.run();
    }
}
