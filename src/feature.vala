/********************************************************************
# Copyright 2017 Daniel 'grindhold' Brendle
#
# This file is part of parceldude.
#
# parceldude is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# parceldude is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with parceldude.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

namespace Parceldude {
    public struct FeatureData {
        public string id;
        public string name;
        public string description;
        public string parent_id;
        public unowned List<FeatureData?> children;
    }

    [GtkTemplate (ui = "/de/grindhold/parceldude/ui/feature.ui")]
    public class Feature : Gtk.Box, FeatureContainer {
        [GtkChild]
        private Gtk.Box childbox;

        [GtkChild]
        private Gtk.Switch active;

        [GtkChild]
        private Gtk.Label title;
        [GtkChild]
        private Gtk.Label description;

        public const int ID = 1;
        public const int PARENT = 2;
        public const int NAME = 3;
        public const int DESCRIPTION = 4;

        public string id {get; private set; default="";}
        public string? parent_id {get; private set; default=null;}

        public Feature(FeatureData f) {
            this.id = f.id;
            this.parent_id = f.parent_id;
            this.title.label = f.name;
            this.description.label = "<i>"+f.description+"</i>";
            this.description.use_markup = true;
            this.show_all();

            this.active.notify["active"].connect(this.switched);

            foreach (FeatureData childdata in f.children) {
                this.add_feature(new Feature(childdata));
            }
        }

        public void add_feature(Feature f) {
            this.childbox.add(f);
        }

        private void switched() {
            this.toggle_down(this.active.active);
        }

        private void toggle_down(bool val) {
            // FIXME: potential endless recursion
            this.active.active = val;
            this.childbox.@foreach((w)=>{
                (w as Feature).toggle_down(val);
            });
        }

        public string get_paramstring() {
            if (this.childbox.get_children().length() > 0) {
                string result = "";
                this.childbox.@foreach((w)=>{
                    string ps = (w as Feature).get_paramstring();
                    result += result.length != 0 && ps.length != 0 ? "," : "";
                    result += ps ;
                });
                return result;
            } else {
                return this.active.active ? this.id : "";
            }
        }
    }
}
