/********************************************************************
# Copyright 2017 Daniel 'grindhold' Brendle
#
# This file is part of parceldude.
#
# parceldude is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# parceldude is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with parceldude.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

namespace Parceldude {
    [GtkTemplate (ui = "/de/grindhold/parceldude/ui/donations.ui")]
    public class DonationsView : Gtk.Box {
        [GtkChild]
        private Gtk.LinkButton linkbutton;

        public signal void done();

        public DonationsView() {
            this.linkbutton.label  = "Support!";
        }

        [GtkCallback]
        public void back() {
            this.done();
        }
        
    }
}
