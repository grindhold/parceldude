/********************************************************************
# Copyright 2017 Daniel 'grindhold' Brendle
#
# This file is part of parceldude.
#
# parceldude is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later
# version.
#
# parceldude is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with parceldude.
# If not, see http://www.gnu.org/licenses/.
*********************************************************************/

namespace Parceldude {
    [GtkTemplate (ui = "/de/grindhold/parceldude/ui/parameter.ui")]
    public class ParameterView : Gtk.Box {
        [GtkChild]
        private Gtk.Box featurelist;

        public ParameterView() {
            this.show_all();
        }

        public void show_installer(Installer? x=null) {
            var old = this.featurelist.get_children();
            if (old.length() > 0)
                this.featurelist.remove(old.nth_data(0));
            if (x != null) {
                this.featurelist.child = x.featurelist;
                this.featurelist.show_all();
            }
        }
    }
}
