Parceldude
==========

![Parceldude Icon](data/icons/hicolor/128x128/apps/parceldude.png)

Parceldude is a tool that enables you to install MSI-Installers without
manually attending them (aeons of pressing Next… Next… Next… Finish… and so on)

Usage
-----

  * Extract the zip archive.
  * Start parceldude.exe as administrator
  * Add installers via the "Add Installers…"-button
  * Configure the installers via the feature view on the right screen side
  * Click "Install all"
  * [magic ensues]

![Parceldude UI screenshot](screenshots/parceldude_normal.png)
![Parceldude UI while installing](screenshots/parceldude_installing.png)



IMPORTANT NOTICE:
-----------------

PLEASE BE AWARE THAT THIS TOOLS PURPOSE IS TO SAVE YOUR PRECIOUS TIME. I ASSUME
YOU KNOW WHAT YOU ARE DOING, SO PARCELDUDE WILL NOT ASK YOU FOR ACCEPTING THE EULA IF
IT IS PRESENT IN THE INSTALLERS YOU CHOOSE BUT ACCEPT IT AUTOMATICALLY FOR YOU! DO NOT
INSTALL ANY INSTALLERS UNKNOWN TO YOU. YOU MIGHT ACCEPT AN EULA THAT YOU DID NOT
WANT TO ACCEPT.

Dependencies
------------

Proudly using some third party-tech:

  * GNOME's msitools
  * Gtk+ 3

License
-------

GPLv3
